## open configurations
ProgramArguments add src/source/polbooks d=true w=false  
d: directed. If network is directed d=true, otherwise d=false  
w: weighted. If network is weighted w=true, otherwise w=false  
For weighted networks third element in each line of data file is weight for the corresponding edge.  
A B 0.1 
B C 0.7 
B D 0.5 
etc. 

## ALGORITHM SCAN(G=<V, E>, ε, μ)

// all vertices in V are labeled as unclassified;  
for each unclassified vertex v ∈ V do  
    // STEP 1. check whether v is a core;  
    if COREε,μ(v) then  
        // STEP 2.1. if v is a core, a new cluster is expanded;  
        generate new clusterID;   
        insert all x ∈ Nε (v) into queue Q;  
        while Q ≠ 0 do  
            y = first vertex in Q;  
            R = {x ∈ V | DirREACHε,μ(y, x)};  
            for each x ∈ R do  
                if x is unclassified or non-member then  
                    assign current clusterID to x;  
                if x is unclassified then  
                    insert x into queue Q;  
            remove y from Q;  
    else  
        // STEP 2.2. if v is not a core, it is labeled as non-member  
        label v as non-member;  
end for.  
// STEP 3. further classifies non-members  
for each non-member vertex v do  
    if (∃ x, y ∈ Γ(v) ( x.clusterID ≠ y.clusterID) then  
        label v as hub  
    else  
        label v as outlier;  
end for.  
end SCAN.  


