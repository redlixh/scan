package source;
/**
 * <p>
 * <p>
 * SCAN: Structural Clustering Algorithm for Networks
 * SCAN is one instance of structural clustering algorithms which basically exploits structural-similarity of vertices to find clusters in networks
 * by nature, it is sort of region-growing algorithm
 * If one vertex has sufficient number of neighbors which are similar enough to it, then a cluster grows around that vertex and enlarges through its neighbors as much as possible
 */


import java.util.*;

public class SCAN implements Constants {

    private Network net;
    // direction for cluster expansion
    private int direction;
    private double eps;
    private int mu;


    public SCAN(Network network) {
        this.net = network;
        this.direction = BIDIRECTIONAL;
    }

    public SCAN(Network network, int direction) {
        this.net = network;
        this.direction = direction;
    }

    // Scan algorithm
    public void run(double eps, int mu) {
        this.eps = eps;
        this.mu = mu;

        int clusterID = 0;
        int curClusterID = 0;
        LinkedList queue = new LinkedList();
        Vertex vertex, yVertex, xVertex;
        HashSet epsNeighborhood = new HashSet<String>();
        HashSet epsNeighborhoodY = new HashSet<String>();

        Iterator itVertex = net.getVertexIterator();

        while (itVertex.hasNext()) {
            vertex = (Vertex) itVertex.next();
            // for each unclassified vertex
            if (vertex.getClusterId() == UNCLASSIFIED) {

                epsNeighborhood = getEpsNeighborhood(vertex, eps);
                // vertex is a core
                if (epsNeighborhood.size() >= mu) {
                    clusterID++;
                    // label new cluster id
                    vertex.setClusterId(clusterID);
                    // iterator vertex ϵ-neighborhood
                    Iterator itEpsNeighbor = epsNeighborhood.iterator();
                    while (itEpsNeighbor.hasNext()) {
                        String epsNeighbor = (String) itEpsNeighbor.next();
                        // insert all x ∈ Nε (v) into queue Q
                        queue.add(epsNeighbor);
                    }
                    while (queue.size() > 0) {    // while queue is not empty
                        // y = first vertex in Q
                        yVertex = (Vertex) net.getVertex((String) queue.removeFirst());
                        yVertex.setClusterId(clusterID);
                        // R = {x ∈ V | DirREACHε,μ(y, x)}
                        epsNeighborhoodY = getEpsNeighborhood(yVertex, eps);
                        Iterator itEpsNeighborY = epsNeighborhoodY.iterator();
                        if (epsNeighborhoodY.size() >= mu) { // y is also a core
                            while (itEpsNeighborY.hasNext()) {
                                String epsNeighborY = (String) itEpsNeighborY.next();
                                xVertex = (Vertex) net.getVertex(epsNeighborY);

                                // if x is unclassified or non-member then
                                // assign current clusterID to x
                                // if x is unclassified then
                                // insert x into queue q

                                if (xVertex.getClusterId() == UNCLASSIFIED) {
                                    queue.add(epsNeighborY);
                                }
                                if (xVertex.getClusterId() == UNCLASSIFIED || xVertex.getClusterId() == NONMEMBER) {
                                    xVertex.setClusterId(clusterID);
                                }
                            }

                        }
                    }

                } // if vertex is core
                else {
                    vertex.setClusterId(NONMEMBER);
                }

            } // if vertex is unclassified
        } // while


        // determine hubs and outliers
        itVertex = net.getVertexIterator();
        while (itVertex.hasNext()) {
            vertex = (Vertex) itVertex.next();
            //System.out.println(vertex.getId() + " = " + ((Integer)vertex.getClusterId()).toString() );

            // non-member vertices are further classified as outliers or hubs
            if (vertex.getClusterId() == NONMEMBER) {
                Set neighbors = vertex.getNeighborhood();
                Iterator itNeighbors = neighbors.iterator();
                Set neighbors_clusters = new HashSet();
                while (itNeighbors.hasNext()) {
                    String neighbor_s = (String) itNeighbors.next();
                    Vertex neighbor = net.getVertex(neighbor_s);
                    neighbors_clusters.add(neighbor.getClusterId());
                }
                // neighbors belong to more than two  different clusters
                if (neighbors.size() > 10 && neighbors_clusters.size() > 2) {
                    vertex.setClusterId(HUB);
                } else {
                    vertex.setClusterId(OUTLIER);
                }
            }
        }


    } // runScan


    //
    public HashSet getEpsNeighborhood(Vertex vertex, double eps) {

        //System.out.println("\nVertex: " + vertex.getLabel() );
        HashSet epsNeighborhood = new HashSet<String>();
        Iterator itNeighbors = vertex.getNeighborhood(this.direction).iterator();
        while (itNeighbors.hasNext()) {
            String neighbor = (String) itNeighbors.next();
            double similarity = vertex.getSimilarity(neighbor);
            //System.out.println("Neighbor: " + neighbor + " sim: " + similarity);
            // get similarity of vertex to its neighbor
            if (similarity >= eps) {
                epsNeighborhood.add(neighbor);
            }
        }
        return epsNeighborhood;

    }


}